import { LitElement, html, css } from 'lit-element';

class TestFetch  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planets: {type:Object}
    };
  }

  constructor() {
    super();
    this.planets = {results()};
  }

  render() {
    return html`
    //<p><code>${this.planet.name}</code><p>
    ${this.planets.results.map((pl) => {
        return html'<div> ${pl.name} ${pl.rotation_period} </div>'
    })}
    `;
  }
  conectedCallback(){
    // super.connectedCallback();
    // try (
    //     this.cargarPlanetas();
    //     ) catch(e) {
    //         alert(e);
    //     }
    }

 cargarPlanetas() {
    fetch("http://swapi.dev/api/planest/1/")
    .then(Response => (
        console.log(response);
         if (response.ok) { throw response; }
         return response.jason();
    ))
    .then(data => {
        this.planets = data;
        console.log(data);
    } )
    .catch(error => {
        alert("problemas con e fetch:" + error);
    })

 }
 
}

customElements.define('test-fetch', TestFetch);