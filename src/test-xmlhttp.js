import { LitElement, html, css } from 'lit-element';

class TestXmslhttp  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planet: {type:String}
    };
  }

  constructor() {
    super();
    this.planet = {};
    this.cargaPrlaneta();
  }

  render() {
    return html`
    <p><code>${this.planet.name}</code><p>
      
    `;
  }
  cargarPlaneta(){
      //xmlHttpRequest
      var req = new XMLHttpRequest()
      req.open('GEt', "http://swapi.dev/api/planest/1/", true);
      req.onreadystatechange =
          ((aEvt) => {
          if (req.readyState === 4) {
              if (req.status === 200) {
                  this.planet = req.responseText;
                  this.requestUpdate();
                  
              }else {
                  alert("Error llamando Rest";)
              }
          }
      }) 
  ;
  req.send(null);

}

customElements.define('test-xmlhttp', TestXmslhttp);