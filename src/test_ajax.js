import { LitElement, html, css } from 'lit-element';

class Test_Ajax  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planet: {type:Object}
    };
  }

  constructor() {
    super();
    this.planet = {};
    this.cargaPlaneta();
  }

  render() {
    return html`
    <p><code>${this.planet.diameter}</code><p>
    `;
  }
  cargarPlaneta() {
      $.get("http://swapi.dev/api/planest/1/", ((data, status) => {
          if (status === "success") {
              this.planet = data;
          } else {
              alert("error llamado rest");
          }
      }));
  }
}

customElements.define('test_ajax', Test_Ajax);